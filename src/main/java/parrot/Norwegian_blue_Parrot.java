package main.java.parrot;

public class Norwegian_blue_Parrot implements Speed_Parrot {
    boolean isNailed;
    double voltage;
    
    Norwegian_blue_Parrot(boolean isNailed,double voltage) {
        this.voltage = voltage;
        this.isNailed = isNailed;
    }
    
    private double getBaseSpeed() {
        return Math.min(24.0, voltage * BasesSpeed);
    }
    
	@Override
	public double parrot_Speed() {
		return (isNailed) ? 0 : getBaseSpeed();
	}

}
