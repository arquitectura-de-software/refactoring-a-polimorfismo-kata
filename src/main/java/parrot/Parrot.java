package main.java.parrot;


public class Parrot {

    private ParrotTypeEnum type;
    private int numberOfCoconuts = 0;
    private double voltage;
    private boolean isNailed;


    public Parrot(ParrotTypeEnum _type, int numberOfCoconuts, double voltage, boolean isNailed) {
        this.type = _type;
        this.numberOfCoconuts = numberOfCoconuts;
        this.voltage = voltage;
        this.isNailed = isNailed;
    }
	
    private Speed_Parrot getParrot() {
		Speed_Parrot parrot = null;    	
    	if (type == ParrotTypeEnum.EUROPEAN) {
    		parrot = new European_Parrot();
		}
    	if (type == ParrotTypeEnum.AFRICAN) {
        	parrot = new African_Parrot(numberOfCoconuts);    	
		}
    	if (type == ParrotTypeEnum.NORWEGIAN_BLUE) {
    	 	parrot = new Norwegian_blue_Parrot(isNailed, voltage);
    	}
    	return parrot;
	}
	
    public double getSpeed() {
		double speed;
    	Speed_Parrot parrot = getParrot();
    	speed = parrot.parrot_Speed();
    	return speed;
    }


}
