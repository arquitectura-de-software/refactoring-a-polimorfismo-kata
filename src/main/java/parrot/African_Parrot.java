package main.java.parrot;

public class African_Parrot implements Speed_Parrot {

	int numberOfCoconuts;
	
	public African_Parrot(int numberOfCoconuts) {
        this.numberOfCoconuts = numberOfCoconuts;
	}
	
    private double getLoadFactor() {
        return 9.0;
    }
	@Override
	public double parrot_Speed() {
		return Math.max(0, BasesSpeed - getLoadFactor() * numberOfCoconuts);
	}

}
